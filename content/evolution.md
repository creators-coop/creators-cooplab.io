---
title: "Competitive Evolution"
subtitle: >
  Users choose among competing improvements,
  directing the evolution of our products.
slug: "evolution"
image: evolution.jpg
date: 2020-04-30T13:03:11-05:00
nextURL: organization/
style: style4
draft: false
---

### Product Names & Trademarks

Our cooperative respects creators' trademarks. Registered works have a product
name and a revision. The product name identifies a major release or variant of
a product. A work's revision is a sequence number used for gratis updates. Any
aspects of the product's name may be subject to trademark. During
registration, the permission to reference a given work, including for
comparison purposes, using its product name is granted. Conversely, other
entities are not given the right to use the product's name, or anything
confusingly similar, for their own works. In our cooperative, trademark is
seen primarily as a way to assure quality to customers, not as a way to hinder
searching or good faith comparison.

This view of trademarks is common in licenses approved by the Open Source
Initiative. For example, the Apache 2.0 license expressly excludes a trademark
grant except as required for reasonable and customary use when describing the
work's origin. Other OSI licenses expressly require that derivative works use
a different name in both marketing and in the implementation. As part of
registration, creators expressly grant permission to use their trademarks
within product evaluations, good-faith product comparisons, and for the
discovery of competitive works.

### Competitive Marketplaces

Our cooperative licensing encourages a competitive market where users may
choose among alternatives for improvements to existing works. Since our
licensing grants cooperative members the right to make a derivative work, a
fork of an existing work under different product name is permitted. This model
permits users to choose the evolution of the works they have grown dependent
upon without having to switch to a completely different market option that
requires a conversion and new training. In open source communities, there are
forks everywhere, but they are most often experiments or local customizations.
It is less common for popular open source projects to be forked with the
express intent of direct competition; it is the implicit threat of a
competitive fork that keeps original developers focused upon the needs of
their customers and collaborators. We expect a similar pattern to emerge in
this context. Since the distribution of forks generates licensing revenue for
its predecessors' creators, even competitive forks are beneficial to the
original creators.

We do not require that derivative works are registered though our cooperative.
Hence, it is possible that a popular work could spawn a proprietary fork whose
creators don't wish to share with our community. Even in this case, sales of
this proprietary fork must include the licensing of the predecessor and
components they incorporate. Moreover, those derivatives would not be endorsed
by our organization, as its users may lose the ability to influence the
evolution of their purchases though a competitive ecosystem. We believe a
vibrant free market ecosystem of competitive improvements will aleviate the
coersive and monopolistic pratices that are so common today.
