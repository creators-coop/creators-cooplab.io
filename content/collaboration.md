---
title: "Market Collaboration"
subtitle: >
    Revenue and coinvestment opportunities abound as we
    build upon one another's works.
slug: "collaboration"
image: collaboration.jpg
date: 2020-04-30T13:03:11-05:00
nextURL: derivative/
style: style2
draft: false
---

### Revenue & Coinvestment

When a work is registered with our cooperative, its developer names a per-unit
price, as well as a total community cost and licensing duration. Copies of the
work are then licensed to the public at the per-unit amount, until the total
community cost or the licensing duration has been reached, upon which the work
is dedicated to the public domain. Similar to open source licensing, the
source code for works registered in this manner become available to
cooperative members who wish to create derivative works.

Unlike creative commons or open source software licensing, our cooperative
license is not free of charge. In exchange for registration, revenue bonds are
issued, which give the holder the right to receive royalties from licensing
revenue. In this way, creators could obtain sustainable financial support from
their works. Moreover, the revenue bonds can be used for coinvestment with
investors and collaborators. The per-unit amount, total community cost, and
licensing duration are not limited by the cooperative, however, they must be
specified with a concrete value. These values may also be downward (but not
upward) adjusted at any time. Besides the individualized unit pricing,
potential buyers and collaborators may use the community cost and license
duration as part of a more collective decision making process.

### Compositional Opportunities

Works often have predecessors and components. When a work has other
dependencies, its per-unit price, community cost, and licensing months are
computed compositionally. Besides dependencies licensed though by the
cooperative, a work's dependencies can also be in the public domain or
compatibility licensed. Our system is compatible with permissive open source
licenses, such as the Apache 2.0 and BSD, that include the rights to make and
distribute derivative works. Licenses having reasonable attribution
requirements, such as the Creative Commons Attribution (CC-BY), may also be
compatible. Most other copyleft and commercial licenses, with restrictions on
use, distribution, or the ability to make derivative works are not compatible
with our approach. Even so, the authors of incompatibly licensed works may
wish to additionally to license their work though our cooperative.

Unlike traditional commercial licensing, our cooperative license does not
restrict the creation of derivative works. In this way, other creators could
build and offer their own improvements in the marketplace, where sales of the
derivative works reward the original developers. Further, users who have
licensed works though our cooperative could contract for their own
improvements. If those derivatives are provided back though the cooperative,
then it is possible for the original developer to incorporate these derivatives
back into the next version of their product. In this way broader commercial
collaboration could be established among creators and their users.

