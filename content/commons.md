---
title: "Evergrowing Commons"
subtitle: >
  As creators earn compensation for their digital works,
  we expand the public domain.
slug: "commons"
image: commons.jpg
date: 2020-04-30T13:03:11-05:00
nextURL: "formation/"
style: style6
draft: false
---

### Political Philosophy

We see creative works as a [common pool resource][CPR] that should not be
subject to exclusive control by creators. Existing copyright law provides two
fundamental benefits to copyright holders: the exclusive right to restrict
distribution, and the exclusive right to make derivative works. We agree that
the former right is necessary to provide economic incentives and economic
support to creators and their financial investors. However, we believe that
the right to make derivatives of a work and the right to use a work in any
sort of application without discrimination are fundamental human rights that
should not be limited. This organization exists to implement a practical
economic policy based upon this fundamental belief.

Our organization engages in political dialogue, just as other organizations
focused on copyright law. Like the Copyright Clearing House or the Software
Publishers Association, we expressly seek economic rewards for creators;
however, we disagree that creators should be granted exclusive control over
derivatives of their works. Like the Free Software Foundation, we strongly
believe that user freedom is fundamental; however, we disagree with the FSF
that distribution of works must necessarily be gratis, or free of charge. Like
the Creative Commons, we seek to build a public commons. That said, the
Creative Commons focuses on gratis distribution while supporting restrictions
on commercial use and derivative works. We propose the inverse, focusing on
the right of use without discrimination and the ability to create derivative
works, instead observing that charging for distribution is essential to
building a market economy that is sustainable for creators and responsive to
users.

### Public Benefit

Our express mission is to expand the public domain though free market action,
providing financial incentives for creators while respecting the rights of the
public to make derivative works and use licensed works without discrimination.
Our focus on expanding the public domain is a central aspect of our model --
by providing a total community cost and licensing duration for each
contribution, a pathway to the public domain is built into every registration.
This mechanism enables new kind of market decision making, where potential
users of a work can take into account not only the short-term licensing price
to them individually, but also the longer-term cost of a work to society.

While users of licensed works and the general public are not voting members,
they are expressly represented in our decision making. Our cooperative is a
public benefit organization, providing public transparency and accountability.
This designation requires directors to act in a manner that is informed and
disinterested with a fiduciary duty to our mission. Hence, directors must not
only consider the economic interests of the organization's membership, they
must also engage with how decisions would affect members of the public.

[CPR]: https://en.wikipedia.org/wiki/Common-pool_resource
