---
title: "Creative Expression"
subtitle: >
  Freedom to create derivative works enables
  new applications that address community needs.
slug: "derivative"
image: derivative.jpg
date: 2020-04-30T13:03:11-05:00
nextURL: evolution/
style: style3
draft: false
---

### Product Diversity

To support creative expression and diverse applications, works listed in our
cooperative include full source code and the rights needed to make and
distribute derivative works. Distribution and use of derivative works
necessarily incorporate the licensing of predecessors and components. In
this way, cooperative members have the ability to alter and remix existing
products and components, sharing their derivative works in a manner
respectful to prior investment. In particular, we do not limit the right to
make a derivative work based upon field of endeavor or intended usage of the
works.

Our approach is similar to Open Source Initiative (OSI) licensing, which
includes the rights to make and distribute derivative works. Also, similar
to OSI guidelines, we do not limit who can make derivatives or what those
derivatives can be. Conversely, our approach is most unlike proprietary
licensing, which typically reserves the exclusive right to make derivative
works; or, if derivative works are permitted, restricts their distribution.
Notably, Creative Commons' restrictions on the creation of derivative works
or commercial use is incompatible with our approach.

### Equitable Market Pricing

Our licensing discriminates based upon application market. When a work is
registered with the cooperative, a per-unit price is specified. As a baseline,
our cooperative interprets each unit as per-seat license for professional
workers in developed countries, such as teachers or doctors. However, not all
markets have licensing scenarios that are so clear. Moreover, sometimes there
are individualized circumstances that may need to be taken into account.
Therefore, while creators can set their per-unit price, they do not have an
individual say in what each licensing unit means; this decision is determined
in a uniform manner by the cooperative, on a market-by-market basis. In this
way we can provide the equitable pricing of compositional works whose
components come from markets that don't anticipate their reuse.

In this manner, our approach is similar to many proprietary licenses, where
pricing is often based upon sensibilities of a given market. Indeed, each kind
of market might have its own pricing guideline that are specific to the
logistics of each given field. This approach violates the Open Source
Initiative definitions since we require payment for distribution and usage,
and, importantly, not all markets and fields of endeavor are treated equally.
Even so, the goals of our market discrimination is to be inclusive while
providing equitable revenue for creators.
