---
title: "Democratic Control"
subtitle: >
  We are owned by creators of digital works and are operated
  for their mutual benefit.
slug: "organization"
image: organization.jpg
date: 2020-04-30T13:03:11-05:00
nextURL: commons/
style: style5
draft: false
---

### Copyright Collective Society

Our cooperative administers content licensing on behalf of our members,
creators of compositional digital works. We provide a shared licensing
infrastructure including: creator membership, composite work registration,
price computation, license fee collection, and royalty distribution. We have a
market licensing committee, which defines markets and sets the meaning of a
unit price within each market. Licensing of works to customers is done though
market specific partners, such as an online store focused on elementary school
educational materials, or an integrator building tailored systems for medical
research teams.

Our organization is similar to other [copyright collectives][CC], such as the
American Society of Composers, Authors and Publishers (ASCAP) or the Copyright
Clearance Center (CCC). The principal difference is that our licensable works
have the potential to be aggregates, with predecessors and components, each of
which could be works of independent creators. Furthermore, our licensable
works may be composed of multiple media types; for example, a complete video
game includes music, graphic arts and software components.

### Cooperative Organization

We organize subject to the [7 cooperative principles][7CP], including
voluntary and open goverance with democratic member control and economic
participation. Our members are creators of digital works, each with one vote.
To be eligible for voting, our members must meet a threshold of economic
activity. Only natural persons may be members, although cooperative
associations may also participate with voting power proportional to their
membership. Our members elect a board of directors, who, in turn appoint
leadership. Additionally, we have a market licensing committee and appeals
panel, whose advisors are directly elected from our members.

Our organization is similar to other cooperatives and member controlled
organizations, such as ASCAP. Unlike ASCAP, we have only one class of members.
Moreover, ASCAP gives multiple votes to members based upon the licensing
revenue that they are responsible for; our members have exactly one vote each.
Unlike some non-profits, where the current board controls eligibility for new
board members, in our cooperative, any member in good standing who meets a
support threshold can be considered for directorship and be on the ballot.

[CC]: https://en.wikipedia.org/wiki/Copyright_collective
[7CP]: https://www.ica.coop/sites/default/files/publication-files/ica-guidance-notes-en-310629900.pdf
