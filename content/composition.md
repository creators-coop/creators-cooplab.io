---
title: "Composite Attribution"
subtitle: >
  As works are integrated to form new compositions,
  contributors are recorded and given credit.
slug: "composition"
image: composition.jpg
date: 2020-04-30T13:03:11-05:00
nextURL: collaboration/
style: style1
draft: false
---

### Compositional Registry

We view digital products as composites. Over time, a given work can have
numerous predecessors, those works from which it is derived. Works may also
incorporate components in which they depend upon for their operation.
Moreover, each of these predecessors and components may themselves have
further dependencies. Even when a work is the result of a solitary creator, it
still relies upon a community that provides baseline knowledge and shared
conventions of expression.

To build a collaborative ecosystem, recording contributions is essential.
Therefore, a work's registration within our cooperative not only lists its
creators, but also names its dependencies, recursively registering them as
needed. Dependencies include predecessors from which the current work is
derived as well as components which are incorporated. Even though there are no
obligations to do so, registration of existing public domain works is
encouraged, so that credit can be given. Moreover, a given registration could
also include references to related materials or other sources of inspiration
suitable for public acknowledgment.

### Acknowledgement Queries

Collectively, these registrations form an acknowledgement graph. For any
registered work, we can generate a manifest that lists its dependencies and
corresponding creators. Conversely, for any creator, we would be able to list
those works to which they have contributed, as well as works that depend upon
their creations. This acknowledgment graph could be queryable so that any
stakeholder would be able to learn more about the people who are responsible
for a given work, opening up new opportunities for recognition and
collaboration.

Aggregate usage statistics can also be generated. As purchase records are
combined with this acknowledgement graph, it is possible to compute how many
copies of a component or predecessor were licensed. These usage statistics can
also be generated for registered public domain works or referenced papers. To
protect the anonymity of buyers, these statistics would be aggregates, perhaps
subtotaled by region or other customer public profile attribute.
